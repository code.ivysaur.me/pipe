# pipe

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A clone of the game Pipe Mania.

Tags: game

## Features

- Setup phase, multi-directional flow, scorekeeping
- "AI Click" mode

## Changelog

2016-05-14 r1
- Initial release
- [⬇️ pipe-r1.zip](dist-archive/pipe-r1.zip) *(3.14 KiB)*

